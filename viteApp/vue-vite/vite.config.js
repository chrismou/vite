const { resolve } = require('path')
export default {
    proxy: {
        '/api': {
            target: 'http://v.juhe.cn',
            changeOrigin: true,
            rewrite: path => path.replace(/^\/api/, '')
        }
    },
    alias: {
        // '@':'./src'
        '/@/': resolve(__dirname, 'src')
    }
}
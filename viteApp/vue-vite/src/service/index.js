import axios from 'axios';
import qs from 'qs';

function getData() {
    axios({
        url: '/api/calendar/year',
        method: 'post',
        data: qs.stringify({
            year: 2020
        }),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(res => {
        return res.data
    })
}
export {
    getData
}